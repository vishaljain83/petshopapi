const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const server = require('../server/index');

describe('routes : pets', () => {
  describe('GET /api/v1/pets', () => {
    it('should respond with all pets', (done) => {
      chai.request(server)
      .get('/api/v1/pets')
      .end((err, res) => {
        // there should be no errors
        should.not.exist(err);
        // there should be a 200 status code
        res.status.should.equal(200);
        // the response should be JSON
        res.type.should.equal('application/json');
        // the JSON response body should have a
        // key-value pair of {"status": "success"}
        res.body.status.should.eql('success');
        // the first object in the data array should
        // have the right keys
        res.body.data[0].should.include.keys(
          'name', 'birthday', 'owner_id'
        );
        done();
      });
    });
  });

  describe('POST /api/v1/pet', () => {
    it('should respond with a success', (done) => {
      chai.request(server)
      .post('/api/v1/pet')
      .send({data: '{"name":"jimmu","birthday":"2014-05-09","owner_id":"2"}'})
      .end((err, res) => {
        // there should be no errors
        should.not.exist(err);
        // there should be a 201 status code
        // (indicating that something was "created")
        res.status.should.equal(201);
        // the response should be JSON
        res.type.should.equal('application/json');
        // the JSON response body should have a
        // key-value pair of {"status": "success"}
        res.body.status.should.eql('success');
        done();
      });
    });
  });
});