const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const server = require('../server/index');

describe('routes : owners', () => {
  describe('GET /api/v1/owners', () => {
    it('should respond with all owners', (done) => {
      chai.request(server)
      .get('/api/v1/owners')
      .end((err, res) => {
        // there should be no errors
        should.not.exist(err);
        // there should be a 200 status code
        res.status.should.equal(200);
        // the response should be JSON
        res.type.should.equal('application/json');
        // the JSON response body should have a
        // key-value pair of {"status": "success"}
        res.body.status.should.eql('success');
        // the first object in the data array should
        // have the right keys
        res.body.data[0].should.include.keys(
          'owner_id','first_name', 'last_name', 'city', 'pet_id'
        );
        done();
      });
    });
  });
});