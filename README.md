## Want to use this project?

1. Fork/Clone
1. Install dependencies - `npm install`
1. Update the user/password in `server/knexfile.js` to use your prefered value if you have modified the vaules while setting up the DB. Default user is `root` and password is `password`
1. To start the server run - `npm start`
1. To run the tests run - `npm test`