const express = require('express');
const app = new express();

app.use(express.json());

// CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
      
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
app.use(allowCrossDomain);

// Routes
const owners = require('./routes/owners');
const pets = require('./routes/pets');

// Register Routes
app.use('/api/v1/owners', owners);
app.use('/api/v1', pets);

let server = app.listen(3001, function () {
    let host = server.address().address;
    let port = server.address().port;
  
    console.log('App listening at http://%s:%s', host, port);
});

module.exports = server;