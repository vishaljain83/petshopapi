const express = require('express');
const router = express.Router();

const knex = require('../knex');

router.get('/', (req, res, next) => {
    knex('owners')
    .join('owner_pet_lks', 'owners.owner_id', '=', 'owner_pet_lks.owner_id')
    .select('owners.owner_id', 'owners.first_name', 'owners.last_name', 'owners.city', 'owner_pet_lks.pet_id')
    .then((owners) => {
        //res.header("Access-Control-Allow-Origin", "*");
        //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.status(200).json({
            status: 'success',
            data: owners
        });
    })
    .catch((err) => {
        res.status(500).json({
            status: 'error',
            data: err
        });
    });
});

module.exports = router;