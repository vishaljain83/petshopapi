const express = require('express');
const router = express.Router();

const knex = require('../knex');

router.get('/pets', (req, res) => {
    knex('pets')
    .join('owner_pet_lks', 'pets.pet_id', '=', 'owner_pet_lks.pet_id')
    .select('pets.name', 'pets.birthday', 'owner_pet_lks.owner_id')
    .then((owners) => {
        //res.header("Access-Control-Allow-Origin", "*");
        //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.status(200).json({
            status: 'success',
            data: owners
        });
    })
    .catch((err) => {
        res.status(500).json({
            status: 'error',
            data: err
        });
    });
});

router.post('/pet', (req, res) => {
    const data = JSON.parse(req.body.data)
    const name = data.name;
    const birthday = data.birthday;
    const owner_id = data.owner_id;

    knex.transaction((t) => {
        return knex("pets")
            .transacting(t)
            .insert({ name: name, birthday: birthday })
            .then((response) => {
            return knex('owner_pet_lks')
                .transacting(t)
                .insert({pet_id: response, owner_id: owner_id})
            })
            .then(t.commit)
            .catch(t.rollback)
    })
    .then((pet) => {
        // transaction suceeded, data written
        res.header("Access-Control-Allow-Origin", "*");
        res.status(201).json({
            status: 'success',
            data: pet
        });
    })
    .catch((err) => {
        // transaction failed, data rolled back
        res.status(500).json({
            status: 'error',
            data: err
        });
    });
});

module.exports = router;